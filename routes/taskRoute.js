// routes/taskRoute.js
// This document contains all the endpoints for our application (also http methods)
const express = require("express");
const router = express.Router();

const taskController = require('../controllers/taskController.js');

// Get all tasks if no query parameter with key name is present
// If query key name is present then query db for task with that name
router.get('/', (req, res) => {
	
	// if query params are present the value of name key is used to get specific task by name
	if (req.query.name) {
		taskController.getTaskUsingName(req.query.name).then(result => {
			res.send(result);
		});
		
	} else {
		// returns result from our controller
		taskController.getAllTask().then(result => {
			res.send(result);
		})

	}


});


// Get task by Id
router.get('/:id', (req, res) => {
		console.log(req.params.id);
		taskController.getTaskUsingId(req.params.id).then(result => {
			res.redirect(result);
		});
});


// Create task
router.post('/', (req, res) => {
	console.log(req.body);
	taskController.createTask(req.body).then(result => {	
		res.redirect('/');
	})
	.catch(err => {
		console.log(err);
	}) 
});

// Complete task using name, name should be in query with key name
router.put('/complete', (req, res) => {
	taskController.completeTaskUsingName(req.query.name).then(result => {
			res.send(result);
		});
});


// Complete task using id as url params
router.put('/:id/complete', (req, res) => {
	taskController.completeTaskUsingId(req.params.id).then(result => {
			res.send(result);
		});
});


router.delete('/:id', (req, res) => {
	taskController.deleteTaskUsingId(req.params.id).then(result => {
		res.send(result);
	});

});


router.put('/:id', (req, res) => {

	taskController.updateTaskById(req.params.id, req.body).then(result => {
		res.send(result);
	})
})





module.exports = router;