// controllers/taskController.js
// This document contains our app feature in displaying and manipulating our database
const Task = require("../models/task.js");

module.exports.getAllTask = () => {

	return Task.find({}).then(result => {
		return result;
	})
	.catch(err => {
		console.log(err);
		return false;
	});

}

module.exports.getTaskUsingName = (name) => {
	return Task.findOne({name: name}).then(result => {
		return result ? result : "No task found.";
	})
	.catch(err => {
		console.log(err);
		return false;
	});
}

module.exports.getTaskUsingId = (id) => {
	return Task.findOne({_id: id}).then(result => {
		return result ? result : "No task found.";
	})
	.catch(err => {
		console.log(err);
		return false;
	});
}

module.exports.completeTaskUsingName = (name) => {
	return Task.updateOne({name: name}, { $set: { status: "complete" }}).then(result => {
		return Task.findOne({name: name}).then(updatedTask => {
					return updatedTask;
				});
	});
}


module.exports.completeTaskUsingId = (id) => {
	return Task.updateOne({_id: id}, { $set: { status: "complete" }}).then(result => {
		return Task.findOne({_id: id}).then(updatedTask => {
					return updatedTask;
		});
	});
}


module.exports.deleteTaskUsingId = (id) => {
	return Task.deleteOne({_id: id}).then(result => {
		return result;
	})
}



module.exports.createTask = (requestBody) => {
	console.log(requestBody.name);
	let newTask = new Task({
		name: requestBody.name
	});

	return newTask.save().then((task, err) => {
		if (err) {
			console.log(err);
			return false; //"Error detected"
		} else {
			return task;
		}
	});

}


module.exports.updateTaskById = (id, newContent) => {
	return Task.findById(id).then((result, err) => {
		if (err) {
			console.log(err);
			return false;
		}

		result.name = newContent.name;

		return result.save().then((updatedTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})


	});
}


