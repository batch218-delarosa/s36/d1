const tasks = fetch('http://localhost:3001/tasks')
.then(response => response.json())
.then(data => {
	return data;
})

tasks.then(result => {
	console.log(result)
	createTaskList(result);
});


function completeTask(id) {
	const urlOptions = {
		method: "PUT",
	}

	return fetch(`http://localhost:3001/tasks/${id}/complete`, urlOptions)
		.then(res => res)
		.then(data => data);
}

// function addTask(taskName) {
// 	const urlOptions = {
// 		method: "POST",
// 		body: JSON.stringify({name: "sample task"})
// 	}

// 	return fetch(`http://localhost:3001/tasks`, urlOptions)
// 		.then(res => res)
// 		.then(data => data);
// }


function createTaskList(taskArray) {
	console.log(taskArray);
	

	for (task of taskArray) {
		if (task.status == "complete") {
			continue;
		}

		let list = document.createElement('li');

		let span = document.createElement('span');
		let btn = document.createElement('button');


		btn.innerHTML = "DONE";

		btn.setAttribute('id', `${task._id}`)

		btn.addEventListener('click', e => {
			console.log(e.target.id);
			const id = e.target.id
			console.log(id);
			// Git put complete task using id
			completeTask(id).then(result => {
				console.log(result);
				window.location.href = "http://localhost:3001";

			})
		})

		span.innerHTML = task.name

		list.appendChild(btn);
		list.appendChild(span);



		document.getElementById('task-id').appendChild(list);

	}


	


} 


document.getElementById('add-task-btn').addEventListener('click', e => {
	const taskName = document.getElementById('task-input').value;

	if (taskName.length <= 0) {
		console.log("Task Name Cannot be blank");
		return;
	}

	console.log(taskName);

	fetch('/tasks', {
		method: "POST",
		headers: {'Content-Type': 'application/json'},
		body: JSON.stringify({name: taskName})
	}).then(res => {
		console.log(res);
		window.location.href = "/";
	})

});