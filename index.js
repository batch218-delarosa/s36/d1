const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const path = require('path');

const dotenv = require('dotenv');
dotenv.config();
const dbURL = process.env.DATABASE_URL;

// models folder >> task.js
// controllers folder >> taskController.js
// route folder >> taskRoute.js

const taskRoute = require("./routes/taskRoute.js");

// Server Setup
const app = express();
const port = 3001;

const taskPagePath = path.join(__dirname,'app');

// Middlewares
app.use('/',express.static(taskPagePath));
app.use(express.json());
app.use(express.urlencoded({extended: true }));
app.use(cors());

// DB connection
mongoose.connect(dbURL,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});


app.use('/tasks', taskRoute);

app.listen(port, () => {
	console.log(`Now listening to port ${port}`);
})